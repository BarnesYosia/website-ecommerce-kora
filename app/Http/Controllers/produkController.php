<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

class produkController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      // $this->middleware('auth');
  }

  /**
   * Show the application Banner.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index(Request $request)
  {
      // dd($banners = Banner::all());
      // if ($request->has('searching')) {
      //     $banners = Banner::where('title','LIKE','%'.$request->searching.'%')->get();
      // }
      // else {
      //
      // }
      $produks = \DB::table('produk')->orderBy('created_at', 'DESC')->get();
      return view('pages/produk',['produks' => $produks]);
  }

  public function indexs(){
      return view('pages/produkcreate');
  }

  // public function logout(){
  //     \Auth::logout();
  //
  //     return redirect()->route('home');
  // }

  public function createProduk(Request $request){

      if ($request->hasFile('gambar')) {
          $gambar = $request->file('gambar');
          $gambarname = time() . '.' . $gambar->getClientOriginalExtension();
          Image::make($gambar)->save('img/'.$gambarname);
      }
      $produks = produk::create(array(
          'nama_barang' => Request('nama_barang'),
          'deskripsi' => Request('deskripsi'),
          'harga' => Request('harga'),
          'id_kategori' => Request('id_kategori'),
          'stok' => Request('stok'),
          'gambar' => $gambarname
      ));
      // dd($produks);
      $produks->save();
      return redirect()->route('produk');
  }

  public function edit($id_produk)
  {
      $produks = Produk::find($id_produk);
      return view ('pages.produkEdit',['produk' => $produks]);
  }

  public function update(Request $request, $id)
  {
      $produks = Produk::find($id_produk);
      $produks->nama_barang = $request->nama_barang;
      $produks->deskripsi = $request->deskripsi;

      if ($request->hasFile('gambar')) {
          $gambar = $request->file('gambar');
          $gambarname = $gambar->getClientOriginalName();
          $gambar->move(public_path('css/img/'),$gambarname);
          $produks->gambar = $request->file('gambar')->getClientOriginalName();
      }

      $produks->update();

      return redirect('produk');
  }
  public function delete($id)
  {
      $produks = Produk::find($id);
      $produks->delete($produks);
      return redirect('produk');
  }
}
