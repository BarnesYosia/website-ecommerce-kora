<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use DB;
use Auth;

class shippingcartController extends Controller
{




  public function index(){

    if (Auth::check()){

      $data['cart'] = DB::table('produk')
      ->join('cart', 'cart.id_produk', '=', 'produk.id_produk')
      ->leftJoin('users', 'users.id', '=', 'produk.nama_barang')
      ->where('cart.id_user', '=', Auth::user()->id)
      ->select('produk.*', 'users.*','cart.*')->get();
      return view ('shippingcart',$data);
    }

    return redirect ('login');
  }

  public function create()
  {
    //
  }


  public function store(Request $request)
  {

    if (Auth::check()){

      $cart = new cart();
      $cart->id_user = Auth::user()->id;
      $cart->id_produk = $request->input('id_produk');
      $cart->quantity = $request->input('quantity');
      $cart->save();
    }

    return redirect('/shippingcart');

  }

  public function show($id)
  {
    //
  }

  public function edit($id)
  {
    //
  }

  public function update(Request $request, $id)
  {
    //
  }

  public function destroy(Request $request, $id_produk)
  {
    $product = cart::where('id_produk',$id_produk);
    $hapus = $product->delete();

    return back();
  }

}
