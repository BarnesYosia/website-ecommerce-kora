<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Province;
use App\City;
use Auth;

class checkoutController extends Controller
{
  public function checkout()
  {
    $cart = \DB::table('produk')
    ->join('cart', 'cart.id_produk', '=', 'produk.id_produk')
    ->leftJoin('users', 'users.id', '=', 'produk.nama_barang')
    ->where('cart.id_user', '=', Auth::user()->id)
    ->select('produk.*', 'users.*','cart.*')->get();
    $city = City::get();
    $province = Province::get();
    $data = array('cart' => $cart, 'city' => $city, 'province' => $province);
    return view ('checkout', $data);
  }

  public function getprovince()
  {
    $client = new Client();

    try{
      $response = $client->get('https://api.rajaongkir.com/starter/province',
        array(
          'headers' => array(
            'key' => 'fb18dd015e6e04fe920ca946bec7ec42',
          )
        )
      );
    }catch(RequestException $e){
      var_dump($e->getResponse()->getBody()->getContents());
    }

    $json = $response->getBody()->getContents();

    $array_result = json_decode($json, true);

    //print_r($array_result);
    //echo $array_result["rajaongkir"]["results"][1]["province"];
    for($i = 0; $i < count($array_result["rajaongkir"]["results"]); $i++)
    {
      $province = new \App\Province;
      $province->id = $array_result["rajaongkir"]["results"][$i]["province_id"];
      $province->name = $array_result["rajaongkir"]["results"][$i]["province"];
      $province->save();
    }
  }

  public function getcity()
  {
    $client = new Client();

    try{
      $response = $client->get('https://api.rajaongkir.com/starter/city',
        array(
          'headers' => array(
            'key' => 'fb18dd015e6e04fe920ca946bec7ec42',
          )
        )
      );
    }catch(RequestException $e){
      var_dump($e->getResponse()->getBody()->getContents());
    }

    $json = $response->getBody()->getContents();

    $array_result = json_decode($json, true);

    //print_r($array_result);
    //echo $array_result["rajaongkir"]["results"][0]["city_name"];
    for($i = 0; $i < count($array_result["rajaongkir"]["results"]); $i++)
    {
      $city = new \App\City;
      $city->id = $array_result["rajaongkir"]["results"][$i]["city_id"];
      $city->name = $array_result["rajaongkir"]["results"][$i]["city_name"];
      $city->id_province = $array_result["rajaongkir"]["results"][$i]["province_id"];
      $city->save();
    }
  }

  public function checkshipping()
  {
    $title = "Check Shipping";
    $city = City::get();

    return view('checkout', compact('title', 'city'));

  }

  public function processShipping(Request $request)
  {
    $title = "Check Shipping Result";
    $client = new Client();

    try{
      $response = $client->request('POST','https://api.rajaongkir.com/starter/cost',
        [
          'body' => 'origin=22&destination='.$request->destination.'&weight='.$request->weight.'&courier=jne',
          'headers' => [
            'key' => 'fb18dd015e6e04fe920ca946bec7ec42',
            'content-type' => 'application/x-www-form-urlencoded',
          ]
        ]
      );
    }catch(RequestException $e){
      var_dump($e->getResponse()->getBody()->getContents());
    }

    $json = $response->getBody()->getContents();

    $array_result = json_decode($json, true);

    $origin = $array_result["rajaongkir"]["origin_details"]["city_name"];
    $origincode = $array_result["rajaongkir"]["origin_details"]["postal_code"];
    $destination = $array_result["rajaongkir"]["destination_details"]["city_name"];
    $destinationcode = $array_result["rajaongkir"]["destination_details"]["postal_code"];

    //print_r($array_result);
    //echo $array_result["rajaongkir"]["results"][0]["costs"][1]["cost"][0]["value"];
    $array = array('array_result' => $array_result, 'origin' => $origin, 'origincode' => $origincode, 'destination' => $destination, 'destinationcode' => $destinationcode);
    return view('checkout-result', $array);
  }
}
