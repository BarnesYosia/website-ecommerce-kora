<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use Faker\Provider\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;


class BeritaController extends Controller
{
    public function index()
    {
        $data['berita'] = \DB::table('berita')->get();
        return view('pages/berita', $data);
    }

    public function create(){
        return view('pages/beritacreate');
    }

    public function store(Request $request){
      if ($request->hasFile('thumbnail')) {
             $thumbnail = $request->file('thumbnail');
             $test = time() . '.' . $thumbnail->getClientOriginalExtension();
             Image::make($thumbnail)->save('uploads/'. $test);


             $berita = Berita::create(array(
                 'title' => request('title'),
                 'deskripsi' => request('deskripsi'),
                 'thumbnail' => $test
         ));
               $berita->save();
         }


         return redirect()->route('berita.index');
             return redirect('/berita')->with('success', 'Data Berhasil Ditambahkan');

     }


    public function edit(Request $request, $id){
        $data['berita'] = \DB::table('berita')->find($id);
        return view('pages/beritacreate', $data);
    }

    public function update(Request $request, $id){
        $rule = [
            'title' => 'required|string',
            'deskripsi' => 'required',
            'thumbnail' => 'required|image|mimes:jpg,png,jpeg,svg'
        ];
        $this->validate($request, $rule);

        $file = $request->file('thumbnail');
        $destinationPath = 'img';
        $file->move($destinationPath, $file);

        $input = $request->all();
        unset($input['_token']);
        unset($input['_method']);

        $status = \DB::table('berita')->where('id', $id)->update($input);

        if($status){
            return redirect('/pages/berita');
        }
        else{
            return redirect('/pages/berita/create');
        }
    }

    public function destroy(Request $request, $id){
        $status = \DB::table('berita')->where('id', $id)->delete();
        if($status){
            return redirect('/berita');
        }
        else{
            return redirect('/pages/berita/create');
        }
    }

    public function detail($id)
    {
        $data['berita'] = \App\berita::where('id', $id)->firstOrFail();
        return view('/blog')->with('berita', $data);
    }
}
