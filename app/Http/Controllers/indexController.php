<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
  public function index()
  {
    $banner = \DB::table('banner')->orderBy('created_at', 'DESC')->paginate(5);
    $produk = \DB::table('produk')->orderBy('created_at', 'DESC')->paginate(4);
    $berita = \DB::table('berita')->orderBy('created_at', 'DESC')->paginate(4);
    $data = array
    (
      'banner'=>$banner,'produk'=>$produk,'berita'=>$berita
    );
    return view ('index',$data);
  }
}
