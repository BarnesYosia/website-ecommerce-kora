<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class newsController extends Controller
{
  public function news(){
    $berita = \DB::table('berita')->orderBy('created_at', 'DESC')->paginate(10);
    return view ('/news', ['berita'=>$berita]);
  }
}
