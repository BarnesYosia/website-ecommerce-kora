<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\App;
use DB;

class DataController extends Controller
{
    //
    public function index()
    {
      $data['banner'] = DB::table('banner')->get();

      return view('index', $data);
    }
}
