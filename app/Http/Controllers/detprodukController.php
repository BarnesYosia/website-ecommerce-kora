<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;

class detprodukController extends Controller
{
  public function detproduk($id_produk){

    $data['product'] =Produk::all()->where('id_produk', $id_produk)->first();

    return view ('detproduk', $data);
  }
}
