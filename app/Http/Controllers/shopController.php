<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class shopController extends Controller
{
  public function shop()
  {
    $produk = \DB::table('produk')->orderBy('created_at', 'DESC')->paginate(10);
    return view ('shop',['produk' => $produk]);
  }
}
