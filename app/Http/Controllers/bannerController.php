<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class bannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application Banner.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        // dd($banners = Banner::all());
        // if ($request->has('searching')) {
        //     $banners = Banner::where('title','LIKE','%'.$request->searching.'%')->get();
        // }
        // else {
        //
        // }
        $banners = \DB::table('banner')->orderBy('created_at', 'DESC')->get();
        return view('pages/banner',['banners' => $banners]);
    }

    public function indexs(){
        return view('pages/bannercreate');
    }

    // public function logout(){
    //     \Auth::logout();
    //
    //     return redirect()->route('home');
    // }

    public function createBanner(Request $request){

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailname = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->save('img/'.$thumbnailname);
        }
        $banners = banner::create(array(
            'title' => request('title'),
            'description' => request('description'),
            'thumbnail' => $thumbnailname
        ));
        $banners->save();
        return redirect()->route('banner');
    }

    public function edit($id)
    {
        $banners = Banner::find($id);
        return view ('pages.banneredit',['banner' => $banners]);
    }

    public function update(Request $request, $id)
    {
        $banners = Banner::find($id);
        $banners->title = $request->title;
        $banners->description = $request->description;

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailname = $thumbnail->getClientOriginalName();
            $thumbnail->move(public_path('css/img/'),$thumbnailname);
            $banners->thumbnail = $request->file('thumbnail')->getClientOriginalName();
        }

        $banners->update();

        return redirect('banner');
    }
    public function delete($id)
    {
        $banners = Banner::find($id);
        $banners->delete($banners);
        return redirect('banner');
    }
}
