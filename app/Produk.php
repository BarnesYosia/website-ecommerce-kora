<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
  protected $table = 'produk';
  protected $primaryKey = 'id_produk';

  protected $fillable = [
    'nama_barang','deskripsi','harga','gambar','id_kategori','stok'
  ];

  public function cart(){
    return $this->hasMany('App/Cart','id_produk');
  }
}
