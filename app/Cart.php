<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $table = 'cart';
    protected $primaryKey = 'id_cart';
    protected $fillable = [
      'id_produk','id_user','quantity'
    ];

    public function product(){
      return $this->belongsTo('App/Produk','id_produk');
    }

    public function user(){
      return $this->belongsTo('App/DataUser','id');
    }
}
