@extends('navbar/navbarUser')

@section('content')

	<div class="colorlib-loader"></div>

	<!-- <div id="page"> -->
		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
			   	<li style="background-image: url({{asset('asset/img/cover-img-6.jpg')}});">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner text-center">
				   					<h1>Product Detail</h1>
				   					<h2 class="bread"><span><a href="{{url('index')}}">Home</a></span> <span><a href="shop.html">Product</a></span> <span>Product Detail</span></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row row-pb-lg">
					<div class="col-md-10 col-md-offset-1">
						<div class="product-detail-wrap">
							<div class="row">
								<div class="col-md-5">
									<div class="product-entry">
										<div class="product-img" style="background-image: url({{url('img/').'/'.$product->gambar}});">
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="desc">
										<h3>{{ $product->nama_barang }}</h3>
										<p class="price">
											<span>{{ $product->harga }}</span>
										</p>
										<p>{{ $product->deskripsi }}</p>
										<div class="color-wrap">
											<p class="color-desc">
												Color:
												<a href="#" class="color color-1"></a>
												<a href="#" class="color color-2"></a>
												<a href="#" class="color color-3"></a>
												<a href="#" class="color color-4"></a>
												<a href="#" class="color color-5"></a>
											</p>
										</div>
										<div class="size-wrap">
											<p class="size-desc">
												Size:
												<a href="#" class="size size-1">xs</a>
												<a href="#" class="size size-2">s</a>
												<a href="#" class="size size-3">m</a>
												<a href="#" class="size size-4">l</a>
												<a href="#" class="size size-5">xl</a>
												<a href="#" class="size size-5">xxl</a>
											</p>
										</div>
										<form type="text" action="{{url('/cartAdd')}}" method="post">
											@csrf
											<input type="text" name="id_produk" value="{{ $product->id_produk }}" hidden>
											<input type="hidden" name="quantity" value="1">
											<button type="submit" class=" btn btn-primary icon-shopping-cart"> Add to Cart</button>
										</form>
										<p><a href="{{url('wishlist')}}" class="btn btn-primary icon-plus2"> Add to Wishlist</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection
