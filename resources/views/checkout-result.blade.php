@extends('navbar/navbarUser')

@section('content')

<div class="colorlib-loader"></div>

<!-- <div id="page"> -->
<aside id="colorlib-hero" class="breadcrumbs">
  <div class="flexslider">
    <ul class="slides">
      <li style="background-image: url(asset/img/cover-img-8.jpg);">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
              <div class="slider-text-inner text-center">
                <h1>Checkout</h1>
                <h2 class="bread"><span><a href="{{url('index')}}">Home</a></span> <span><a href="{{url('shippingcart')}}">Shopping Cart</a></span> <span>Checkout</span></h2>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</aside>

<div class="colorlib-shop">
  <div class="container">
    <div class="row row-pb-md">
      <div class="col-md-10 col-md-offset-1">
        <div class="process-wrap">
          <div class="process text-center active">
            <p><span>01</span></p>
            <h3>Shopping Cart</h3>
          </div>
          <div class="process text-center active">
            <p><span>02</span></p>
            <h3>Checkout</h3>
          </div>
          <div class="process text-center">
            <p><span>03</span></p>
            <h3>Order Complete</h3>
          </div>
        </div>
      </div>
    </div>

    <table width="300" cellpading="5" cellspacing="5" style="border-collapse:collapse;">
      <tr>
        <td><h5>Dari</h5></td>
        <td><h5>:</h5></td>
        <td><h5>{{$origin}} {{$origincode}}</h5></td>
      </tr>
      <tr>
        <td><h5>Tujuan</h5></td>
        <td><h5>:</h5></td>
        <td><h5>{{$destination}} {{$destinationcode}}</h5></td>
      </tr>
    </table>

    <table class="table table-hover" width="300" cellpading="5" style="border-collapse:collapse;">
      <thead>
        <tr>
          <th>Layanan</th>
          <th>Deskripsi</th>
          <th>Tarif Ongkir</th>
          <th>Estimasi</th>
          <th>Action</th>
        </tr>
      </thead>
      <?php
      foreach($array_result['rajaongkir']['results'] as $key => $value){
        ?>
        <h4><?php print $value['name']; ?></h5>
          <?php
          foreach($value['costs'] as $keys => $values){
            ?>
            <tbody>
              <tr>
                <td><?php print $values['service']; ?></td>
                <td><?php print $values['description']; ?></td>
                <td>Rp.<?php print $values['cost'][0]['value']; ?>,00</td>
                <td><?php print $values['cost'][0]['etd']; ?> hari</td>
                <td><label><input type="checkbox" value=""> Pilih</label></td>
              </tr>
          <?php
        }
      }
      ?>
    </tbody>
  </table>
      <div class="cart-detail">
        <h2>Payment Method</h2>
        <div class="form-group">
          <div class="col-md-12">
            <div class="radio">
              <label><input type="radio" name="optradio">Direct Bank Tranfer</label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="radio">
              <label><input type="radio" name="optradio">Check Payment</label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="radio">
              <label><input type="radio" name="optradio">Paypal</label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox">
              <label><input type="checkbox" value="">I have read and accept the terms and conditions</label>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <p><a href="#" class="btn btn-success" style="width:200px;margin-left:940px">Bayar</a></p>
        </div>
      </div>
    </div>
  </div>
  @endsection
