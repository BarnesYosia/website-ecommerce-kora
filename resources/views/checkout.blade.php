@extends('navbar/navbarUser')

@section('content')

	<div class="colorlib-loader"></div>

	<!-- <div id="page"> -->
		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
			   	<li style="background-image: url(asset/img/cover-img-8.jpg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner text-center">
				   					<h1>Checkout</h1>
				   					<h2 class="bread"><span><a href="{{url('index')}}">Home</a></span> <span><a href="{{url('shippingcart')}}">Shopping Cart</a></span> <span>Checkout</span></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col-md-10 col-md-offset-1">
						<div class="process-wrap">
							<div class="process text-center active">
								<p><span>01</span></p>
								<h3>Shopping Cart</h3>
							</div>
							<div class="process text-center active">
								<p><span>02</span></p>
								<h3>Checkout</h3>
							</div>
							<div class="process text-center">
								<p><span>03</span></p>
								<h3>Order Complete</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-7">
						<form method="post" class="colorlib-form" action="{{route('processShipping')}}">
							{{ csrf_field() }}
							<h2>Billing Details</h2>
		              	<div class="row">
											<div class="form-group">
 									<!-- <div class="col-md-6">
 										<label for="fname">Your Name</label>
 										<input type="text" id="fname" class="form-control" placeholder="Name here">
 									</div> -->
 									<!-- <div class="col-md-6">
 										<label for="lname">Last Name</label>
 										<input type="text" id="lname" class="form-control" placeholder="Your lastname">
 									</div> -->
 								</div>
										 <div class="col-md-12">
			                  <div class="form-group">
			                  	<label for="country">Select Destination</label>
			                     <div class="form-field">
			                     	<i class="icon icon-arrow-down3"></i>
			                        <select name="destination" id="destination" class="form-control">
				                      	<option selected="selected" value="">Select City</option>
																@foreach($city as $c)
																<option value="{{ $c->id }}">{{ $c->name }}</option>
																@endforeach
			                        </select>
			                     </div>
			                  </div>
			               </div>
			               <div class="form-group">
									<div class="col-md-6">
										<label for="stateprovince">Weight</label>
										<input name="weight" type="text" id="weight" class="form-control" placeholder="Weight">
									</div>
									<!-- <div class="col-md-6">
										<label for="lname">Zip/Postal Code</label>
										<input type="text" id="zippostalcode" class="form-control" placeholder="Zip / Postal">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6">
										<label for="email">E-mail Address</label>
										<input type="text" id="email" class="form-control" placeholder="Your Email">
									</div>
									<div class="col-md-6">
										<label for="Phone">Phone Number</label>
										<input type="text" id="zippostalcode" class="form-control" placeholder="">
									</div> -->
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<div class="radio">
										  <label><input type="radio" name="optradio">Create an Account? </label>
										  <label><input type="radio" name="optradio"> Ship to different address</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<p><button type="submit" class="btn btn-primary">Submit</button></p>
									</div>
								</div>
		              </div>
		            </form>
					</div>
					<div class="col-md-5">
					 <div class="cart-detail">
							<h2>Cart Total</h2>
							<ul>
								<li>
									<ul>
										<?php $total = 0 ?>

										@foreach( $cart as $row)
										<?php $total += ((int)$row->harga * (int)$row->quantity) ?>
										<li><span>{{ $row->quantity }} x {{ $row->nama_barang }}</span><span>Rp.{{ $row->harga}}</span></li>
										@endforeach
										<li><span style="margin-top:20px">Subtotal</span> <span style="margin-top:20px">Rp.{{ $total }}</span></li>
									</ul>
								</li>
							</ul>
						</div>

						 <!-- <div class="cart-detail">
							<h2>Payment Method</h2>
							<div class="form-group">
								<div class="col-md-12">
									<div class="radio">
									   <label><input type="radio" name="optradio">Direct Bank Tranfer</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="radio">
									   <label><input type="radio" name="optradio">Check Payment</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="radio">
									   <label><input type="radio" name="optradio">Paypal</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<div class="checkbox">
									   <label><input type="checkbox" value="">I have read and accept the terms and conditions</label>
									</div>
								</div>
							</div>
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<p><a href="{{url('confirmation')}}" class="btn btn-primary">Place an order</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->

		<!-- <div id="colorlib-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-6 text-center">
							<h2><i class="icon-paperplane"></i>Sign Up for a Newsletter</h2>
						</div>
						<div class="col-md-6">
							<form class="form-inline qbstp-header-subscribe">
								<div class="row">
									<div class="col-md-12 col-md-offset-0">
										<div class="form-group">
											<input type="text" class="form-control" id="email" placeholder="Enter your email">
											<button type="submit" class="btn btn-primary">Subscribe</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div> -->
@endsection
