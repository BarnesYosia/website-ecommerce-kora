@extends('navbar/navbarUser')

@section('content')

	<div class="colorlib-loader"></div>
		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
			   	<li style="background-image: url(asset/img/cover-img-7.jpg);">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner text-center">
				   					<h1>Shopping Cart</h1>
				   					<h2 class="bread"><span><a href="{{url('index')}}">Home</a></span> <span><a href="{{url('shop')}}">Product</a></span> <span>Shopping Cart</span></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row row-pb-md">
					<div class="col-md-10 col-md-offset-1">
						<div class="process-wrap">
							<div class="process text-center active">
								<p><span>01</span></p>
								<h3>Shopping Cart</h3>
							</div>
							<div class="process text-center">
								<p><span>02</span></p>
								<h3>Checkout</h3>
							</div>
							<div class="process text-center">
								<p><span>03</span></p>
								<h3>Order Complete</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="row row-pb-md">
					<div class="col-md-10 col-md-offset-1">
						<div class="product-name">
							<div class="one-forth text-center">
								<span>Product Details</span>
							</div>
							<div class="one-eight text-center">
								<span>Price</span>
							</div>
							<div class="one-eight text-center">
								<span>Jumlah Barang</span>
							</div>
							<div class="one-eight text-center">
								<span>Total</span>
							</div>
							<div class="one-eight text-center">
								<span>Remove</span>
							</div>
						</div>
						<?php $total = 0 ?>

						@foreach($cart as $row)
						<?php $total += ((int)$row->harga * (int)$row->quantity) ?>

						<div class="product-cart">
							<div class="one-forth">
								<div class="product-img" style="background-image: url({{url('img/').'/'.$row->gambar}});">
								</div>
								<div class="display-tc">
									<h3>{{ $row->nama_barang}}</h3>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">Rp.{{ $row->harga}}</span>
								</div>
							</div>
							<div class="one-eight text-center">
							<input type="number" name="quantity" min="1" max="50s" value="{{ $row->quantity }}" min class="form-control" />
						</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<span class="price">Rp.{{ ((int)$row->harga * (int)$row->quantity) }}</span>
								</div>
							</div>
							<div class="one-eight text-center">
								<div class="display-tc">
									<form class="" action="{{url('/shippingcart', $row->id_produk)}}" method="post">
										@method('DELETE')
										@csrf
										<button type="submit" class="btn btn-danger" name="button">Delete</button>
									</form>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="total-wrap">
							<div class="row">
								<div class="col-md-8">
									<form action="#">
										<div class="row form-group">
											<div class="col-md-9">
												<input type="text" name="quantity" class="form-control input-number" placeholder="Your Coupon Number...">
											</div>
											<div class="col-md-3">
												<input type="submit" value="Apply Coupon" class="btn btn-primary">
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-3 col-md-push-1 text-center">
									<div class="total">
										<div class="sub">
											<p><span>Subtotal:</span> <span>Rp.{{ $total }}</span></p>
										</div>
										<div class="grand-total">
											<p><span><strong>Total:</strong></span> <span>Rp.{{ $total }}</span></p>
										</div>
									</div>
								</div>
								<div class="row" style="margin-left:790px">
									<div class="col-md-12">
										<p><a href="{{url('checkout')}}" class="btn btn-primary">Checkout</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection
