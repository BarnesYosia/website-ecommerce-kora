@extends('navbar/navbar')

@section('produk')
<div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <i class="fa fa-align-justify"></i>produk</div>
                        <div class="" style="margin-left:20px">
                            <div class="row mb-2" style="margin-top:20px">
                              <div class="col-sm-6">
                                  <a href="{{ url('/createe') }}">Add</a>
                              </div><!-- /.col -->
                            </div><!-- /.row -->
                          </div><!-- /.container-fluid -->
                      <div class="card-body">
                        <table class="table table-responsive-s table-hover">
                          <thead>
                            <tr>
                              <th>Thumbnail</th>
                              <th>Title</th>
                              <th>Description</th>
                              <th>Stok</th>
                              <!-- <th>Created at</th> -->
                              <th>Action</th>
                            </div>
                            </tr>
                          </thead>
                          <tbody>
                              @foreach ($produks as $produk)
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-5 col-xs-1">
                                                <img src="{{asset('img/'. $produk->gambar)}}" class="img-fluid img-thumbnail" style="img-table" alt="sheep" width="200px">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="">{{$produk->nama_barang}}</td>
                                    <td>{{$produk->deskripsi}}</td>
                                    <td>{{$produk->stok}}</td>

                                    <td>
                                        <a href="{{url('produk/edit/',$produk->id_produk)}}" class="btn btn-warning btn-m">Edit</a>
                                        <a href="{{url('produk/delete',['id_produk'=>$produk->id_produk])}}" class="btn btn-danger btn-m" onclick="return confirm(
                                            'Are you sure you want to delete this produk ?')" >Delete</a>
                                    </td>
                                </tr>
                                  @endforeach
                          </tbody>
                        </table>

@endsection
