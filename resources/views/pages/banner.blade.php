@extends('navbar/navbar')

@section('banner')
@isset($success)
<script type="text/javascript">
  alert('Sukses gan');
</script>
@endisset
<div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <i class="fa fa-align-justify"></i>Banner</div>
                        <div class="" style="margin-left:20px">
                            <div class="row mb-2" style="margin-top:20px">
                              <div class="col-sm-6">
                                  <a href="{{ url('/creates') }}">Add</a>
                              </div><!-- /.col -->
                            </div><!-- /.row -->
                          </div><!-- /.container-fluid -->
                      <div class="card-body">
                        <table class="table table-responsive-s table-hover">
                          <thead>
                            <tr>
                              <th>Thumbnail</th>
                              <th>Title</th>
                              <th>Description</th>
                              <!-- <th>Created at</th> -->
                              <th>Action</th>
                            </div>
                            </tr>
                          </thead>
                          <tbody>
                              @foreach ($banners as $banner)
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-5 col-xs-1">
                                                <img src="{{asset('img/'. $banner->thumbnail)}}" class="img-fluid img-thumbnail" style="img-table" alt="sheep" width="200px">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="">{{$banner->title}}</td>
                                    <td>{{$banner->description}}</td>

                                    <td>
                                        <a href="{{route('banner.edit',['id' => $banner->id])}}" class="btn btn-warning btn-m">Edit</a>
                                        <a href="{{route('banner.delete',['id'=> $banner->id])}}" class="btn btn-danger btn-m" onclick="return confirm(
                                            'Are you sure you want to delete this banner ?')" >Delete</a>
                                    </td>
                                </tr>
                                  @endforeach
                          </tbody>
                        </table>

@endsection
