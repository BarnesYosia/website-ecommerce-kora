@extends('navbar/navbar')

@section('datauser')
<div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <i class="fa fa-align-justify"></i>users</div>
                        <div class="" style="margin-left:20px">
                            <div class="row mb-2" style="margin-top:20px">
                            </div><!-- /.row -->
                          </div><!-- /.container-fluid -->
                      <div class="card-body">
                        <table class="table table-responsive-s table-hover">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>Email</th>
                            </div>
                            </tr>
                          </thead>
                          <tbody>
                              @foreach ($datausers as $users)
                                <tr>
                                    <td>{{$users->name}}</td>
                                    <td>{{$users->email}}</td>
                                </tr>
                                  @endforeach
                          </tbody>
                        </table>

@endsection
