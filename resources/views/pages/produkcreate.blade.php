@extends('navbar/navbar')

@section('produk')

<html>
<head>
    <title>Form Produk</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data Produk</h1>
    <form action="{{ url('/produk/create') }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if(!empty($produk))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="id_produk">ID produk</label>
            <input type="text" class="form-control" name="id_produk" value="{{ old('id_produk', @$produk->id_produk) }}" placeholder="Masukkan ID" readonly>
            <br>
            <label for="nama_produk">Nama Produk</label>
            <input type="text" class="form-control" name="nama_barang" value="{{ old('nama_barang', @$produk->nama_barang) }}" placeholder="Masukkan Nama Produk">
            <br>
            <label for="deskripsi">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" cols="20" rows="10" id="placeOfDeath" value="{{ old('deskripsi', @$produk->deskripsi) }}" placeholder="Masukkan Deskripsi"></textarea>
            <br>
            <label for="harga">Harga</label>
            <input type="text" class="form-control" name="harga" value="{{ old('harga', @$produk->harga) }}" placeholder="Masukkan Harga"></textarea>
            <br>
            <label for="stok">Stok</label>
            <input type="text" class="form-control" name="stok" value="{{ old('stok', @$produk->stok) }}" placeholder="Masukan Stok Barang"></textarea>
            <br>
            <label for="kategori_id">Kategori</label>
            <input type="text" class="form-control" name="id_kategori" value="{{ old('id_kategori', @$produk->id_kategori) }}" placeholder="(1)Men,(2)Woman"></textarea>
            <br>
        </div>
        <div class="form-group">
            <label for="email">Cover</label>
            <div>
                <img width="100" height="100"/>
                <input type="file" class="uploads form-control" name="gambar">
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>
@endsection
