@extends('navbar/navbarUser')

@section('content')

	<div class="colorlib-loader"></div>

	<!-- <div id="page"> -->
		<aside id="colorlib-hero" class="breadcrumbs">
			<div class="flexslider">
				<ul class="slides">
			   	<li style="background-image: url({{asset('asset/img/cover-img-10.jpg')}});">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner text-center">
				   					<h1>Our News/Blog</h1>
				   					<h2 class="bread"><span><a href="{{url('index')}}">Home</a></span> <span>News</span></h2>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
			  	</ul>
		  	</div>
		</aside>

		<div class="colorlib-blog">
			<div class="container">
				<div class="row">
					@foreach($berita as $beritas)
					<div class="col-md-4">
						<article class="article-entry">
							<a href="{{url('blog/'.$beritas->id.'/detail')}}" class="blog-img" style="background-image: url({{url('img/').'/'.$beritas->thumbnail}});"></a>
							<div class="desc">
								<h2><a href="{{url('blog/'.$beritas->id.'/detail')}}">{{ $beritas->title }}</a></h2>
							</div>
						</article>
					</div>
				@endforeach
				</div>
			</div>
		</div>

		<div id="colorlib-subscribe">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="col-md-6 text-center">
							<h2><i class="icon-paperplane"></i>Sign Up for a Newsletter</h2>
						</div>
						<div class="col-md-6">
							<form class="form-inline qbstp-header-subscribe">
								<div class="row">
									<div class="col-md-12 col-md-offset-0">
										<div class="form-group">
											<input type="text" class="form-control" id="email" placeholder="Enter your email">
											<button type="submit" class="btn btn-primary">Subscribe</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection
