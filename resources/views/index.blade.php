@extends('navbar/navbarUser')

@section('content')

	<body>

	<div class="colorlib-loader"></div>

	<!-- <div id="page"> -->

		<aside id="colorlib-hero">
			<div class="flexslider">
				<ul class="slides">
					@foreach($banner as $banners)
			   	<li style="background-image: url({{url('img/').'/'.$banners->thumbnail}});">
			   		<div class="overlay"></div>
			   		<div class="container-fluid">
			   			<div class="row">
				   			<div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">
				   				<div class="slider-text-inner">
				   					<div class="#">
					   					<h1 class="head-1">{{$banners->title}}</h1>
					   					<h2 class="head-2">{{$banners->description}}</h2>
					   					<p class="head-2 p-3 mb-2 bg-dark text-white"><span>New stylish outfits</span></p>
					   					<p><a href="{{url('forman')}}" class="btn btn-primary">Shop Collection</a></p>
				   					</div>
				   				</div>
				   			</div>
				   		</div>
			   		</div>
			   	</li>
					@endforeach
			  	</ul>
		  	</div>
		</aside>
		<div id="colorlib-featured-product">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<a href="{{url('shop')}}" class="f-product-1" style="background-image: url(asset/img/item-17.jpg);">
							<div class="desc">
								<h2>Fashion <br>for <br>men</h2>
							</div>
						</a>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<a href="{{url('shop')}}" class="f-product-2" style="background-image: url(asset/img/item-18.jpg);">
									<div class="desc">
										<h2>New <br>Arrival <br>Dress</h2>
									</div>
								</a>
							</div>
							<div class="col-md-6">
								<a href="{{url('shop')}}" class="f-product-2" style="background-image: url(asset/img/item-19.jpg);">
									<div class="desc">
										<h2>Sale <br>20% <br>off</h2>
									</div>
								</a>
							</div>
							<div class="col-md-12">
								<a href="{url('shop')}" class="f-product-2" style="background-image: url(asset/img/item-20.jpg);">
									<div class="desc">
										<h2>Shoes <br>for <br>Unisex</h2>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="colorlib-shop">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>New Arrival</span></h2>
						<p>We love to tell our successful far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url(asset/img/item-21.jpg);">
								<a class=""><span class="tag badge badge-success">New</span></a>
								<div class="cart">
									<p>
										<span class="addtocart"><a href="{{url('shippingcart')}}"><i class="icon-shopping-cart"></i></a></span>
										<span><a href="{{url('detproduk')}}"><i class="icon-eye"></i></a></span>
										<span><a href="#"><i class="icon-heart3"></i></a></span>
										<span><a href="{{url('wishlist')}}"><i class="icon-bar-chart"></i></a></span>
									</p>
								</div>
							</div>
							<div class="desc">
								<h3><a href="{{url('shop')}}">Floral Dress</a></h3>
								<p class="price"><span>$300.00</span></p>
							</div>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url(asset/img/item-22.jpg);">
								<p class="tag"><span class="new">New</span></p>
								<div class="cart">
									<p>
										<span class="addtocart"><a href="{{url('shippingcart')}}"><i class="icon-shopping-cart"></i></a></span>
										<span><a href="{{url('detproduk')}}"><i class="icon-eye"></i></a></span>
										<span><a href="#"><i class="icon-heart3"></i></a></span>
										<span><a href="{{url('wishlist')}}"><i class="icon-bar-chart"></i></a></span>
									</p>
								</div>
							</div>
							<div class="desc">
								<h3><a href="{{url('shop')}}">Floral Dress</a></h3>
								<p class="price"><span>$300.00</span></p>
							</div>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url(asset/img/item-23.jpg);">
								<p class="tag"><span class="new">New</span></p>
								<div class="cart">
									<p>
										<span class="addtocart"><a href="{{url('shippingcart')}}"><i class="icon-shopping-cart"></i></a></span>
										<span><a href="{{url('detproduk')}}"><i class="icon-eye"></i></a></span>
										<span><a href="#"><i class="icon-heart3"></i></a></span>
										<span><a href="{{url('wishlist')}}"><i class="icon-bar-chart"></i></a></span>
									</p>
								</div>
							</div>
							<div class="desc">
								<h3><a href="{{url('shop')}}">Floral Dress</a></h3>
								<p class="price"><span>$300.00</span></p>
							</div>
						</div>
					</div>
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url(asset/img/item-25.jpg);">
								<p class="tag"><span class="new">New</span></p>
								<div class="cart">
									<p>
										<span class="addtocart"><a href="{{url('shippingcart')}}"><i class="icon-shopping-cart"></i></a></span>
										<span><a href="{{url('detproduk')}}"><i class="icon-eye"></i></a></span>
										<span><a href="#"><i class="icon-heart3"></i></a></span>
										<span><a href="{{url('wishlist')}}"><i class="icon-bar-chart"></i></a></span>
									</p>
								</div>
							</div>
							<div class="desc">
								<h3><a href="{{url('shop')}}">Floral Dress</a></h3>
								<p class="price"><span>$300.00</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="colorlib-intro" class="colorlib-intro" style="background-image: url(asset/img/cover-img-2.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="intro-desc">
							<div class="text-salebox">
								<div class="text-lefts">
									<div class="sale-box">
										<div class="sale-box-top">
											<h2 class="number">45</h2>
											<span class="sup-1">%</span>
											<span class="sup-2">Off</span>
										</div>
										<h2 class="text-sale">Sale</h2>
									</div>
								</div>
								<div class="text-rights">
									<h3 class="title">Just hurry up limited offer!</h3>
									<p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
									<p><a href="{{url('shop')}}" class="btn btn-primary">Shop Now</a> <a href="#" class="btn btn-primary btn-outline">Read more</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="colorlib-shop">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>Our Products</span></h2>
						<p>Our product has been used for fashion week in seoul, we so glad to be participant on Seoul Fashion Week.</p>
					</div>
				</div>
				<div class="row">
					@foreach($produk as $produks)
					<div class="col-md-3 text-center">
						<div class="product-entry">
							<div class="product-img" style="background-image: url({{url('img/').'/'.$produks->gambar}});">
								<!-- <p class="tag"><span class="sale">Sale</span></p> -->
								<div class="cart">
									<p>
										<span><a type="submit"><i class="icon-shopping-cart"></i></a></span>
										<span><a href="{{url('detproduk')}}"><i class="icon-eye"></i></a></span>
										<span><a href="#"><i class="icon-heart3"></i></a></span>
										<span><a href="{{url('wishlist')}}"><i class="icon-bar-chart"></i></a></span>
									</p>
								</div>
							</div>
							<div class="desc">
								<h3><a href="{{url('shop')}}">{{$produks->nama_barang}}</a></h3>
								<p class="price"><span>Rp.{{$produks->harga}}</span></p>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>

		<div id="colorlib-testimony" class="colorlib-light-grey">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
						<h2><span>Our Satisfied Customer says</span></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="owl-carousel2">
							<div class="item">
								<div class="testimony text-center">
									<span class="img-user" style="background-image: url(asset/img/person1.jpg);"></span>
									<span class="user">Alysha Myers</span>
									<small>Miami Florida, USA</small>
									<blockquote>
										<p>" A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
									</blockquote>
								</div>
							</div>
							<div class="item">
								<div class="testimony text-center">
									<span class="img-user" style="background-image: url(asset/img/person2.jpg);"></span>
									<span class="user">James Fisher</span>
									<small>New York, USA</small>
									<blockquote>
										<p>One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
									</blockquote>
								</div>
							</div>
							<div class="item">
								<div class="testimony text-center">
									<span class="img-user" style="background-image: url(asset/img/person3.jpg);"></span>
									<span class="user">Jacob Webb</span>
									<small>Athens, Greece</small>
									<blockquote>
										<p>Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="colorlib-blog">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center colorlib-heading">
						<h2>Recent News</h2>
						<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name</p>
					</div>
				</div>
				<div class="row">
					@foreach($berita as $beritas)
					<div class="col-md-4">
						<article class="article-entry">
							<a href="{{url('blog/'.$beritas->id.'/detail')}}" class="blog-img" style="background-image: url({{url('img/').'/'.$beritas->thumbnail}});"></a>
							<div class="desc">
								<h2><a href="{{url('blog/'.$beritas->id.'/detail')}}">{{$beritas->title}}</a></h2>
							</div>
						</article>
					</div>
					@endforeach
				</div>
			</div>
		</div>

	<!-- </div> -->

	</body>
@endsection
