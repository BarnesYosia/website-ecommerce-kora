<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});



Route::get('/dash', function () {
    return view('pages.dashboard');
});

Route::get('/banner', function () {
    return view('pages.banner');
});
Route::get('/banner', 'bannerController@index')->name('banner');
Route::get('/index', 'DataController@index')->name('index');
Route::post('/banner/create', 'bannerController@createBanner')->name('banner.createBanner');
Route::get('/creates', 'bannerController@indexs')->name('creates');
Route::get('/banner/edit/{id}', 'bannerController@edit')->name('banner.edit');
// Route::post('/banner/edit/{id}/update', 'bannerController@update')->name('banner.update');
Route::post('/banner/update/{id}', 'bannerController@update')->name('banner.update');
Route::get('/banner/delete/{id}', 'bannerController@delete')->name('banner.delete');

Route::get('/berita', function () {
    return view('pages.berita');
});
Route::get('/berita', 'beritaController@index')->name('berita.index');
Route::post('/berita/create', 'beritaController@store')->name('berita.store');
Route::get('/create', 'beritaController@create')->name('create');
Route::get('/berita/edit/{id}', 'beritaController@edit')->name('berita.edit');
Route::post('/berita/edit/{id}/update', 'beritaController@update')->name('berita.update');
Route::delete('/berita/delete/{id}', 'beritaController@destroy')->name('berita.destroy');

Route::get('/produk', function () {
    return view('pages.produk');
});
Route::get('/produk', 'produkController@index')->name('produk');
Route::post('/produk/create', 'produkController@createProduk')->name('produk.createProduk');
Route::get('/createe', 'produkController@indexs')->name('createe');
Route::get('/produk/edit/{id_produk}', 'produkController@edit')->name('produk.edit');
Route::post('/produk/edit/{id_produk}/update', 'produkController@update')->name('produk.update');
Route::get('/produk/delete/{id_produk}', 'produkController@delete')->name('produk.delete');

// Route::get('/datauser', 'dataUserController@')
Route::get('/dataUser', 'dataUserController@index')->name('dataUser');

Route::get('/index', 'indexController@index')->name('index');
Route::get('/shop', 'shopController@shop');
Route::get('/detproduk/{id_produk}', 'detprodukController@detproduk');
Route::get('/shippingcart', 'shippingcartController@index')->name('shippingcart.index');

Route::get('/checkout', 'checkoutController@checkout');
Route::get('/getprovince', 'checkoutController@getprovince');
Route::get('/getcity', 'checkoutController@getcity');
Route::get('/checkshipping', 'checkoutController@checkshipping');
Route::post('/processShipping', 'checkoutController@processShipping')->name('processShipping');
Route::post('/checkout-result', 'checkoutController@checkout-result');

Route::get('/wishlist', 'wishlistController@wishlist');
Route::get('/news', 'newsController@news');
Route::get('/about', 'aboutController@about');
Route::get('/contact', 'contactController@contact');
Route::get('/confirmation', 'confirmationController@confirmation');
Route::get('/forman', 'formanController@forman');
Route::get('/forwoman', 'forwomanController@forwoman');
Route::get('/blog', 'blogController@blog');
Route::get('/blog/{id}/detail', 'beritaController@detail')->name('/blog');

Route::post('/cartAdd', 'shippingcartController@store')->name('shippingcart.store');
Route::delete('/shippingcart/{id_produk}', 'shippingcartController@destroy')->name('product.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/postlogin','LoginController@postlogin');



Route::get('/home', 'HomeController@index')->name('home');
